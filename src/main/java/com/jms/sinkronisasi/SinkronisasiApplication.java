package com.jms.sinkronisasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class SinkronisasiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinkronisasiApplication.class, args);
	}

}
