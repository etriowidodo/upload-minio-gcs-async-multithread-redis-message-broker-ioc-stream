package com.jms.sinkronisasi.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class StringUtils {
    public String convertDecimalToAscii(String intString) {
        String[] intArray = intString.split(",");
        int[] result = new int[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            result[i] = Integer.parseInt(intArray[i]);
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < result.length; i++) {
            char c = (char) result[i];
            sb.append(Character.toString(c));
        }
        return sb.toString();
    }


    public List compareColumn(String allColumnSatker,String allColumnPusat){
        String[] arrayAllColumnSatker = allColumnSatker.replaceAll("\\[|\\]", "").split(",");
        String[] arrayAllColumnPusat  = allColumnPusat.replaceAll("\\[|\\]", "").split(",");
        List finalColumn = new ArrayList<>();
        for(String columnSatker : arrayAllColumnSatker){
            for(String columnPusat : arrayAllColumnPusat){
                if(columnSatker.trim().equals(columnPusat.trim())){
                    finalColumn.add(columnSatker);
                }
            }
        }

        return finalColumn;
    }

    public String replaceBracket(String stringBracket){
        String replaced = stringBracket
                            .replace("[","(")
                            .replace("]",")")
                            .replaceAll("\"", "'");
        return  replaced;
    }

    public String replaceBracketUpdate(String stringBracket){
        String replaced = stringBracket
                .replace("[","")
                .replace("]","")
                .replaceAll("\"", "'");
        return  replaced;
    }






}
