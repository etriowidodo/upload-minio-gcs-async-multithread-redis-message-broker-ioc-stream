package com.jms.sinkronisasi.utils;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
@Slf4j
public class JSONUtils {
    public List getAllKeys(Iterator<String> fieldNames) {
        List listKey = new ArrayList();
        while (fieldNames.hasNext()) {
            String fieldName = fieldNames.next();
            listKey.add(fieldName);
        }

        return listKey;
    }
}
