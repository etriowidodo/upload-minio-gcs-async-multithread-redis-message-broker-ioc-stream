package com.jms.sinkronisasi.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.*;

@Component
@Slf4j
public class DBUtils {
    @Value("${spring.datasource.url}")
    private String db;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    public Connection getConnection(String db, String username, String password) throws SQLException {
        Connection conn = DriverManager.getConnection(db, username, password);
        if (conn != null) {
            return conn;
        }else{
            return null;
        }
    }

    public Map<String, Object> getData(String sql, String header) throws SQLException{
        Map<String, Object> hasil = new HashMap<>();
        Connection conn = this.getConnection(db, username, password);
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql);
        List<Map<String, Object>> rows = new ArrayList<>();
        ResultSetMetaData rsmd = result.getMetaData();
        int columnCount = rsmd.getColumnCount();

        while (result.next()) {
            // Represent a row in DB. Key: Column name, Value: Column value
            Map<String, Object> row = new HashMap<>();
            for (int i = 1; i <= columnCount; i++) {
                // Note that the index is 1-based
                String colName = rsmd.getColumnName(i);
                Object colVal = result.getObject(i);
                row.put(colName, colVal);
            }
            rows.add(row);
        }
        hasil.put(header, rows);
        conn.close();
        return hasil;
    }

    public Set getColumn(String sql) throws SQLException{
        Map<String, Object> hasil = new HashMap<>();
        Connection conn = this.getConnection(db, username, password);
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(sql);
        List<Map<String, Object>> rows = new ArrayList<>();
        ResultSetMetaData rsmd = result.getMetaData();
        int columnCount = rsmd.getColumnCount();

        Set colum = new HashSet();
            for (int i = 1; i <= columnCount; i++) {
                // Note that the index is 1-based
                String colName = rsmd.getColumnName(i);
                colum.add(colName);
            }
        conn.close();
        return colum;
    }

    public void insertUpdateData(String sql) throws SQLException {
        Connection conn = this.getConnection(db, username, password);
        Statement statement = conn.createStatement();
        statement.executeUpdate(sql);
        conn.close();
    }

}
