package com.jms.sinkronisasi.controller;

import com.jms.sinkronisasi.service.minio.MinioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/minio")
public class MinioController {

    @Autowired
    private MinioService minioService;

    @PostMapping("/createBucket")
    public ResponseEntity<String> createBucket(@RequestParam("bucketName") String bucketName) {
        try {
            minioService.createBucket(bucketName);
            return ResponseEntity.ok("Bucket created successfully: " + bucketName);
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Failed to create bucket: " + e.getMessage());
        }
    }

    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,String bucketName,String path) throws Exception {
        minioService.createBucket(bucketName);
        String fileName = path+'/'+file.getOriginalFilename();
        minioService.uploadFile(bucketName, fileName, file);
        return "File uploaded successfully!";
    }


    @GetMapping("/download")
    public String downloadFiles(
            @RequestParam String bucketName,
            @RequestParam String path
    ) {
        try {
            List<String> files = minioService.listFiles(bucketName, path);
            String baseDestinationPath   = "D:\\KERJAAN\\KEJAGUNG-Daskrimti\\2023\\sinkronisasi\\miniodownload\\";
            String destinationDirectory  = baseDestinationPath+path;
            Path directoryPath = Paths.get(destinationDirectory );
            if (!Files.exists(directoryPath)) {
                Files.createDirectories(directoryPath);
            }
            for (String file : files) {
                String destinationPath = baseDestinationPath  + "\\" + file;
                minioService.downloadFile(bucketName, file, destinationPath);
            }

            return "Files downloaded successfully!";
        } catch (Exception e) {
            // Handle exceptions
            e.printStackTrace();
            return "Failed to download files: " + e.getMessage();
        }
    }



}
