package com.jms.sinkronisasi.controller;

import com.jms.sinkronisasi.local.model.dao.DownloadBackupChunk;
import com.jms.sinkronisasi.local.model.repository.DownloadBackupChunkRepository;
import com.jms.sinkronisasi.service.download.DownloadBackupService;
import com.jms.sinkronisasi.service.minio.MinioService;
import com.jms.sinkronisasi.utils.CacheUtil;
import lombok.var;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.IntStream;

import static com.jms.sinkronisasi.constant.Constants.BACKUP_PATH_MINIO;
import static com.jms.sinkronisasi.constant.Constants.FILE_NAME;

@RestController
@RequestMapping("/chunk-receiver")
public class FileController {

    @Autowired
    MinioService minioService;

    @Autowired
    DownloadBackupService downloadBackupService;
    private static final String UPLOAD_DIRECTORY = "E:\\gdrive\\download\\testupload";

    @Autowired
    protected CacheUtil cacheUtil;

    @PostMapping("/receive")
    public String receiveChunk(@RequestParam int chunkIndex, @RequestBody byte[] chunkData) {
        try {
            String fileName = "jdk-8u281-windows-x64.exe"; // Name of the file to be reconstructed
            File chunkFile = new File(UPLOAD_DIRECTORY, fileName + "_part_" + chunkIndex);

            try (FileOutputStream fos = new FileOutputStream(chunkFile)) {
                fos.write(chunkData);
            }

            return "Chunk " + chunkIndex + " received successfully";
        } catch (IOException e) {
            e.printStackTrace();
            return "Failed to receive chunk " + chunkIndex;
        }
    }

    @GetMapping("/downloadDataFromMinio")
    public String downloadDataFromMinio(@RequestParam String bucketName, @RequestParam String path) throws IOException {
        var count = minioService.countFilesInPath(bucketName,path);
        downloadBackupService.downloadBackupMinio(count,bucketName,path);
        return String.valueOf(count);
    }

    @GetMapping("/test")
    public String test() {
        cacheUtil.putCache("TestRedis","google","ini contoh isi");
        return "hallo";
    }
    @GetMapping("/assemble")
    public String assembleChunks() {
        String fileName = "jdk-8u281-windows-x64.exe";
        Path assembledFilePath = Paths.get(UPLOAD_DIRECTORY, fileName);

        try (FileOutputStream fos = new FileOutputStream(assembledFilePath.toFile())) {
            for (int i = 0; ; i++) {
                File chunkFile = new File(UPLOAD_DIRECTORY, fileName + "_part_" + i);
                if (!chunkFile.exists()) {
                    break;
                }

                byte[] chunkData = Files.readAllBytes(chunkFile.toPath());
                fos.write(chunkData);
                chunkFile.delete(); // Delete the chunk after assembling
            }

            return "File assembled successfully";
        } catch (IOException e) {
            e.printStackTrace();
            return "Failed to assemble file";
        }
    }
    @GetMapping("/assemble-pdf")
    public String assembleChunksPdf() {
        String fileName = FILE_NAME;
        Path assembledFilePath = Paths.get(UPLOAD_DIRECTORY, fileName);

        try {
            PDFMergerUtility pdfMerger = new PDFMergerUtility();
            int i = 0;
            while (true) {
                File chunkFile = new File(UPLOAD_DIRECTORY, fileName + "_part_" + i);
                if (!chunkFile.exists()) {
                    break;
                }
                pdfMerger.addSource(chunkFile);
                i++;
            }

            pdfMerger.setDestinationFileName(assembledFilePath.toString());
            pdfMerger.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());

            return "File assembled successfully";
        } catch (IOException e) {
            e.printStackTrace();
            return "Failed to assemble file";
        }
    }

}
