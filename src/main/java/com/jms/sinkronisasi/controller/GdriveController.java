package com.jms.sinkronisasi.controller;

import com.jms.sinkronisasi.service.gcs.GCloudStorageService;
//import com.jms.sinkronisasi.service.gdrive.GdriveService;
import com.jms.sinkronisasi.service.gdrive.GdriveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
@RequestMapping("/gdrive")
public class GdriveController {

    @Autowired
    GdriveService gdriveService;
    @Autowired
    GCloudStorageService gCloudStorageService;

    @GetMapping("/upload")
    public String handleFileUpload() {
        try {
            String localFilePath = "E:\\backup\\backup.sql"; // Replace this with your local file path
            File fileToUpload = new File(localFilePath);
            gdriveService.uploadFileTest(fileToUpload);
            return "File uploaded successfully!";
        } catch (IOException | GeneralSecurityException e) {
            return "Failed to upload file: " + e.getMessage();
        }
    }

    @GetMapping("/upload2")
    public String handleFileUpload2() {
        try {
            String localFilePath = "E:\\backup\\backup.sql"; // Replace this with your local file path
            File fileToUpload = new File(localFilePath);
            gdriveService.uploadFile2(fileToUpload);
            return "File uploaded successfully!";
        } catch (IOException | GeneralSecurityException e) {
            return "Failed to upload file: " + e.getMessage();
        }
    }

    @GetMapping("/createBucket")
    public String createBucket() throws GeneralSecurityException, IOException {
        gCloudStorageService.createBucketGcs();
        return "Bucket Create successfully!";
    }

    @GetMapping("/upload3")
    public String handleFileUpload3() {
        try {
            String localFilePath = "E:\\gdrive\\download\\testupload\\backup_db_cms\\10.45\\backup.sql"; // Replace this with your local file path
            File fileToUpload = new File(localFilePath);
            gCloudStorageService.uploadFileToBucket(fileToUpload);
            return "File uploaded successfully!";
        } catch (IOException | GeneralSecurityException e) {
            return "Failed to upload file: " + e.getMessage();
        }
    }


}