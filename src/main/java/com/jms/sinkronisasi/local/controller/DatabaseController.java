package com.jms.sinkronisasi.local.controller;

import com.jms.sinkronisasi.local.service.BackupService;
import com.jms.sinkronisasi.service.minio.FileService;
import com.jms.sinkronisasi.service.minio.UploadChunkGdriveService;
import com.jms.sinkronisasi.service.minio.UploadChunkMinioService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.DecimalFormat;

@RestController
@RequestMapping("/database")
public class DatabaseController {

    @Autowired
    BackupService backupService;
    @Autowired
    FileService fileService;
    @Autowired
    UploadChunkMinioService uploadChunkMinioService;

    @Autowired
    UploadChunkGdriveService uploadChunkGdriveService;


    @GetMapping("/backup")
    public String backup() throws Exception {

        backupService.BackupDb();

        return "succsess";
    }

    @GetMapping("/upload")
    public String upload() throws Exception {
        uploadChunkMinioService.uploadChunkToMinio();

        return "succsess";
    }

    @GetMapping("/uploadGdrive")
    public String uploadGdrive() throws Exception {
        uploadChunkGdriveService.uploadChunkToGdrive();

        return "succsess";
    }

    @GetMapping("/uploadGdrive2")
    public String uploadGdrive2() throws Exception {
        uploadChunkGdriveService.uploadChunkToGdrive();

        return "succsess";
    }
    @GetMapping("/progres-upload")
    public String progresUpload() throws Exception {
        var progress = fileService.checkProgressBackup();
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            return String.valueOf(decimalFormat.format(progress));
    }

}
