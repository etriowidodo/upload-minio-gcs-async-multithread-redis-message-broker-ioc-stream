package com.jms.sinkronisasi.local.model.repository;

import com.jms.sinkronisasi.local.model.dao.BackupChunk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BackupChunkRepository extends JpaRepository<BackupChunk, Long> {

    List<BackupChunk> findByStatusOrderByPartAsc(boolean status);
    @Query(value = "SELECT COUNT(*) AS total FROM backup_chunk", nativeQuery = true)
    long getTotalCount();

    @Query(value = "SELECT COUNT(*) AS total FROM backup_chunk WHERE status = true", nativeQuery = true)
    long getTrueCount();

}
