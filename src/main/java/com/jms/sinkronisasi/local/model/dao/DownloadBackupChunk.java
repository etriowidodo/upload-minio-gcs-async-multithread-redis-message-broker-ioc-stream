package com.jms.sinkronisasi.local.model.dao;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "download_backup_chunk")
public class DownloadBackupChunk {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "bucket_name")
    private String bucketName;
    @Column(name = "full_filename")
    private String fullFilename;
    @Column(name = "path_upload")
    private String pathUpload;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "part")
    private int part;

    @Column(name = "status")
    private boolean status;
}
