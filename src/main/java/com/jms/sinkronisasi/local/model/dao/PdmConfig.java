package com.jms.sinkronisasi.local.model.dao;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Data
@Entity
@Table(name = "pdm_config", schema = "pidum")
public class PdmConfig {

    @Id
    @Column(name = "kd_satker", nullable = false)
    private String kdSatker;

    @Column(name = "time_format", columnDefinition = "bpchar default 'WIB'")
    private String timeFormat;

    @Column(name = "flag")
    private Character flag;

    @Column(name = "kode_surat")
    private String kodeSurat;

    @Column(name = "p_tinggi")
    private String pTinggi;

    @Column(name = "p_negeri")
    private String pNegeri;

    @Column(name = "alamat_p_tinggi", columnDefinition = "text")
    private String alamatPTinggi;

    @Column(name = "alamat_p_negeri", columnDefinition = "text")
    private String alamatPNegeri;

    @Column(name = "no_urut_rp")
    private Short noUrutRp;

    @Column(name = "no_urut_rt")
    private Short noUrutRt;

    @Column(name = "no_urut_rb")
    private Short noUrutRb;

    @Column(name = "format_rp")
    private String formatRp;

    @Column(name = "format_rt")
    private String formatRt;

    @Column(name = "format_rb")
    private String formatRb;

}
