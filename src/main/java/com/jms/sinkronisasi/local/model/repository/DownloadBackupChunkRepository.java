package com.jms.sinkronisasi.local.model.repository;

import com.jms.sinkronisasi.local.model.dao.BackupChunk;
import com.jms.sinkronisasi.local.model.dao.DownloadBackupChunk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DownloadBackupChunkRepository extends JpaRepository<DownloadBackupChunk, Long> {

    List<DownloadBackupChunk> findByStatusOrderByPartAsc(boolean status);
    @Query(value = "SELECT COUNT(*) AS total FROM download_backup_chunk", nativeQuery = true)
    long getTotalCount();

    @Query(value = "SELECT COUNT(*) AS total FROM download_backup_chunk WHERE status = true", nativeQuery = true)
    long getTrueCount();

}
