package com.jms.sinkronisasi.local.model.repository;

import com.jms.sinkronisasi.local.model.dao.PdmConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PdmConfigRepository extends JpaRepository<PdmConfig, String> {
    PdmConfig findFirstByOrderByKdSatkerAsc();
}
