package com.jms.sinkronisasi.local.service;

import com.jms.sinkronisasi.local.model.repository.PdmConfigRepository;
import com.jms.sinkronisasi.service.minio.FileService;
import com.jms.sinkronisasi.service.minio.MinioService;
import com.jms.sinkronisasi.service.minio.UploadChunkMinioService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;

import static com.jms.sinkronisasi.constant.Constants.BACKUP_PATH_MINIO;
import static jdk.nashorn.internal.objects.NativeMath.log;

@Service
public class BackupService {

    @Value("${backup.db.host}")
    String hostname;
    @Value("${spring.datasource.username}")
    String username;
    @Value("${backup.db.name}")
    String dbName;
    @Value("${backup.db.port}")
    String dbPort;
    @Value("${backup.db.directory}")
    String backupDirectory;
    @Value("${spring.datasource.password}")
    String password;
    @Value("${backup.db.dump.location}")
    String pgdum;

    @Autowired
    PdmConfigRepository pdmConfigRepository;

    @Autowired
    MinioService minioService;

    @Autowired
    FileService fileService;

    @Autowired
    UploadChunkMinioService uploadChunkMinioService;

    @Async
    public void BackupDb() throws Exception {
        var pdmConfigData   = pdmConfigRepository.findFirstByOrderByKdSatkerAsc();
        String pathDirChunk = backupDirectory+ File.separator+pdmConfigData.getKdSatker()+ File.separator+"backup";
        String nameFile     = BACKUP_PATH_MINIO;
        String filePath     = backupDirectory + File.separator + nameFile;
        String bucketName   = pdmConfigData.getKdSatker();
        File directory = new File(backupDirectory);
        if (!directory.exists()) {
            boolean created = directory.mkdirs();
            if (created) {
                System.out.println("Backup directory created successfully.");
            }
        }

        File directoryChunk = new File(pathDirChunk);
        if (!directoryChunk.exists()) {
            boolean created = directoryChunk.mkdirs();
            if (created) {
                System.out.println("Backup directory chunk file created successfully.");
            }
        }

        minioService.createBucket(bucketName);

        Runtime r = Runtime.getRuntime();
        ProcessBuilder pb;
        Process p;
        r = Runtime.getRuntime();
        pb = new ProcessBuilder(pgdum + "pg_dump.exe", "-f",filePath ,
                "-F", "c", "-Z", "9", "-v", "-o", "-h", hostname, "-p", dbPort, "-U", username, dbName);
        pb.environment().put("PGPASSWORD", password);
        pb.environment().put("PGPASSWORD", password);
        pb.redirectErrorStream(true);
        p = pb.start();
        try {
            InputStream is          = p.getInputStream();
            OutputStream out        = p.getOutputStream();
            InputStreamReader isr   = new InputStreamReader(is);
            BufferedReader br       = new BufferedReader(isr);
            String ll;
            while ((ll = br.readLine()) != null) {

                    System.out.println(ll);
            }

        } catch (IOException e) {
            log("ERROR " + e.getMessage(), e);
        }
        System.out.println("backup selesai");
        fileService.chunkFile(filePath,nameFile,pathDirChunk,bucketName);
       // uploadChunkMinioService.uploadChunkToMinio();
    }

}
