package com.jms.sinkronisasi.jms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jms.sinkronisasi.utils.DBUtils;
import com.jms.sinkronisasi.utils.JSONUtils;
import com.jms.sinkronisasi.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class TestListener {

    @Autowired
    StringUtils stringUtils;
    @Autowired
    JSONUtils jsonUtils;

    @Autowired
    DBUtils dbUtils;
    @JmsListener(destination = "produser_daerah")
    public void handleMessage(String message) {

        String finalMessage = stringUtils.convertDecimalToAscii(message);
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode jsonNode = mapper.readTree(finalMessage);
            String status = jsonNode.get("status").asText();
            System.out.println(status);
            if(status.equals("insert")){
                this.insert(jsonNode);
            }else if(status.equals("delete")){
                this.delete(jsonNode);
            }else if(status.equals("update")){
                this.update(jsonNode);
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Received message from myQueue: " + finalMessage);
    }

    public void insert(JsonNode jsonNode) throws SQLException {
        String allColumnSatker  = jsonUtils.getAllKeys(jsonNode.get("data").fieldNames()).toString();
        String schemaName       = jsonNode.get("schema").asText();
        String tableName        = jsonNode.get("table").asText();
        List finalColumn        = this.finalColumn(allColumnSatker,schemaName,tableName);
        String data             =  jsonNode.get("data").asText();
        List AllColumnValue = new ArrayList();
        for(Object column : finalColumn) {
            AllColumnValue.add(jsonNode.get("data").get(column.toString().trim()));
        }
        String query = "INSERT INTO "+schemaName+"."+tableName+" "
                        +stringUtils.replaceBracket(finalColumn.toString())+" VALUES "
                        +stringUtils.replaceBracket(AllColumnValue.toString());
        dbUtils.insertUpdateData(query);
        System.out.println(query);
        System.out.println(AllColumnValue.toString());
        System.out.println("ini final column"+finalColumn);
        System.out.println(allColumnSatker);
        System.out.println(jsonNode.get("data"));

    }

    public void delete(JsonNode jsonNode) throws SQLException {
        String schemaName       = jsonNode.get("schema").asText();
        String tableName        = jsonNode.get("table").asText();
        String allColumnPk      = jsonUtils.getAllKeys(jsonNode.get("pk").fieldNames()).toString();
        List listAllColumnPk     = jsonUtils.getAllKeys(jsonNode.get("pk").fieldNames());
        List AllColumnPkValue = new ArrayList();
        for(Object column : listAllColumnPk) {
            AllColumnPkValue.add(jsonNode.get("pk").get(column.toString().trim()));
        }
        String query = "DELETE from "+schemaName+"."+tableName+" where "
                        +stringUtils.replaceBracket(allColumnPk)+" IN "
                        +"("+stringUtils.replaceBracket(AllColumnPkValue.toString())+")";
        dbUtils.insertUpdateData(query);
        System.out.println(query);
        System.out.println(allColumnPk);
        System.out.println(AllColumnPkValue);
    }

    public void update(JsonNode jsonNode) throws SQLException {
        String allColumnSatker  = jsonUtils.getAllKeys(jsonNode.get("data").fieldNames()).toString();
        String schemaName       = jsonNode.get("schema").asText();
        String tableName        = jsonNode.get("table").asText();
        String allColumnPk      = jsonUtils.getAllKeys(jsonNode.get("pk").fieldNames()).toString();
        List listAllColumnPk    = jsonUtils.getAllKeys(jsonNode.get("pk").fieldNames());

        List finalColumn        = this.finalColumn(allColumnSatker,schemaName,tableName);
        List AllColumnPkValue = new ArrayList();
        for(Object column : listAllColumnPk) {
            AllColumnPkValue.add(jsonNode.get("pk").get(column.toString().trim()));
        }

        List AllColumnValue = new ArrayList();

        for(Object columnValue : finalColumn) {
            AllColumnValue.add(columnValue+"="+jsonNode.get("data").get(columnValue.toString().trim()));
        }
        String query = "UPDATE "+schemaName+"."+tableName+" SET "+
                stringUtils.replaceBracketUpdate(AllColumnValue.toString())+" where "
                +stringUtils.replaceBracket(allColumnPk)+" IN "
                +"("+stringUtils.replaceBracket(AllColumnPkValue.toString())+")";
        dbUtils.insertUpdateData(query);
        System.out.println(query);
        System.out.println(AllColumnValue.toString());
        System.out.println(allColumnPk);
        System.out.println(AllColumnPkValue);
    }

    public List finalColumn(String allColumnSatker,String schemaName,String tableName) throws SQLException {
        String sqlGetColumnt    = "select * from "+schemaName+"."+tableName;
        String allColumnPusat   = dbUtils.getColumn(sqlGetColumnt).toString();
        List finalColumn        = stringUtils.compareColumn(allColumnSatker,allColumnPusat);
        return finalColumn;
    }
}
