package com.jms.sinkronisasi.service.gdrive;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;


@Service
public class GdriveService {



    private static final String CREDENTIALS_FILE_PATH_OAUTH2 = "credoauth2.json";
    private final ResourceLoader resourceLoader;

    private static final String CREDENTIALS_FILE_PATH = "cred.json";
    private static final String APPLICATION_NAME = "daknf";

    public GdriveService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    private Drive getDriveService() throws IOException, GeneralSecurityException {
        InputStream credentialsInputStream = getClass().getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH);

        if (credentialsInputStream == null) {
            throw new FileNotFoundException("File not found: " + CREDENTIALS_FILE_PATH);
        }

        GoogleCredential credential = GoogleCredential.fromStream(credentialsInputStream)
                .createScoped(Collections.singleton(DriveScopes.DRIVE));

        return new Drive.Builder(
                GoogleNetHttpTransport.newTrustedTransport(),
                GsonFactory.getDefaultInstance(),
                credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    private Drive getDriveService2() throws IOException, GeneralSecurityException {
        InputStream credentialsInputStream = getClass().getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH_OAUTH2);

        if (credentialsInputStream == null) {
            throw new FileNotFoundException("File not found: " + CREDENTIALS_FILE_PATH_OAUTH2);
        }
        GoogleCredential credential = GoogleCredential.fromStream(credentialsInputStream);
        return new Drive.Builder(
                GoogleNetHttpTransport.newTrustedTransport(),
                GsonFactory.getDefaultInstance(),
                credential)
                .setApplicationName("APPLICATION_NAME")
                .build();
    }

    // Method untuk mengunggah file ke Google Drive
    public void uploadFile(java.io.File fileToUpload) throws IOException, GeneralSecurityException {
        Drive driveService = getDriveService();
        File folderTest = createFolder(driveService,"TestTerUPloadBro");

        File fileMetadata = new File();
        fileMetadata.setName(fileToUpload.getName());
        fileMetadata.setParents(Collections.singletonList("1EeGjz6GWijh_btXd8LLO5qYrCLrQhfe8"));
        FileContent mediaContent = new FileContent("mime/type", fileToUpload);

        File uploadedFile = driveService.files().create(fileMetadata, mediaContent)
                .setFields("id")
                .execute();

        System.out.println("File uploaded: " + uploadedFile.getId());
        File uploadedFileSuccsees = getFileById(uploadedFile.getId());
        System.out.println("File Name: " + uploadedFileSuccsees.getName());

        shareFile(driveService, uploadedFile.getId(), "etriowidodo@gmail.com");
        shareFolder(driveService,folderTest.getId(),"etriowidodo@gmail.com");
    }

    public void uploadFileTest(java.io.File fileToUpload) throws IOException, GeneralSecurityException {
        Drive driveService = getDriveService();
        File folderTest = createFolder(driveService,"IniDirectoryTest");

        File fileMetadata = new File();
        fileMetadata.setName(fileToUpload.getName());
        fileMetadata.setParents(Collections.singletonList(folderTest.getId()));
        FileContent mediaContent = new FileContent("mime/type", fileToUpload);

        File uploadedFile = driveService.files().create(fileMetadata, mediaContent)
                .setFields("id")
                .execute();

        System.out.println("Directory id : " + folderTest.getId());
        File uploadedFileSuccsees = getFileById(uploadedFile.getId());
        System.out.println("File Name: " + uploadedFileSuccsees.getName());

        shareFile(driveService, uploadedFile.getId(), "etriowidodo@gmail.com");
        shareFolder(driveService,folderTest.getId(),"etriowidodo@gmail.com");
    }

    public void uploadFile2(java.io.File fileToUpload) throws IOException, GeneralSecurityException {
        Drive driveService = getDriveService2();

        File fileMetadata = new File();
        fileMetadata.setName(fileToUpload.getName());

        FileContent mediaContent = new FileContent("mime/type", fileToUpload);

        File uploadedFile = driveService.files().create(fileMetadata, mediaContent)
                .setFields("id")
                .execute();

        System.out.println("File uploaded: " + uploadedFile.getId());
        File uploadedFileSuccsees = getFileById(uploadedFile.getId());
        System.out.println("File Name: " + uploadedFileSuccsees.getName());
        File folderTest = createFolder(driveService,"UploadSpring");
        shareFile(driveService, uploadedFile.getId(), "etriowidodo@gmail.com");
        shareFolder(driveService,folderTest.getId(),"etriowidodo@gmail.com");
    }

    public File getFileById(String fileId) throws IOException, GeneralSecurityException {
        Drive driveService = getDriveService();
        return driveService.files().get(fileId).execute();
    }

    public static File createFolder(Drive driveService, String folderName) throws IOException {
        File folderMetadata = new File();
        folderMetadata.setName(folderName);
        folderMetadata.setMimeType("application/vnd.google-apps.folder");

        return driveService.files().create(folderMetadata)
                .setFields("id, name")
                .execute();
    }

    private static void shareFile(Drive driveService, String fileId, String userEmail) throws IOException {
        Permission userPermission = new Permission()
                .setType("user")
                .setRole("reader")
                .setEmailAddress(userEmail);

        driveService.permissions().create(fileId, userPermission).execute();
        System.out.println("File shared with user: " + userEmail);
    }

    public static void shareFolder(Drive driveService, String folderId, String userEmail) throws IOException {
        Permission userPermission = new Permission()
                .setType("user")
                .setRole("writer") // Change the role as needed
                .setEmailAddress(userEmail);

        driveService.permissions().create(folderId, userPermission)
                .execute();

        System.out.println("Folder shared with user: " + userEmail);
    }

    private static String getFolderId(Drive driveService, String folderName) throws IOException {
        String pageToken = null;
        String folderId = null;

        do {
            FileList result = driveService.files().list()
                    .setQ("mimeType='application/vnd.google-apps.folder' and name='" + folderName + "'")
                    .setSpaces("drive")
                    .setFields("files(id)")
                    .setPageToken(pageToken)
                    .execute();

            List<File> files = result.getFiles();
            if (files != null && !files.isEmpty()) {
                folderId = files.get(0).getId();
                break;
            }
            pageToken = result.getNextPageToken();
        } while (pageToken != null);

        return folderId;
    }
}
