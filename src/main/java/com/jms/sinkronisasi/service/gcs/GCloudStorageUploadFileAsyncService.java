package com.jms.sinkronisasi.service.gcs;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;
import com.jms.sinkronisasi.local.model.dao.BackupChunk;
import com.jms.sinkronisasi.local.model.repository.BackupChunkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.GeneralSecurityException;

@Service
public class GCloudStorageUploadFileAsyncService {
    private static final String CREDENTIALS_FILE_PATH = "cred.json";
    @Autowired
    BackupChunkRepository backupChunkRepository;
    private static Storage  getStorage() throws IOException, GeneralSecurityException {
        InputStream credentialsInputStream = GCloudStorageUploadFileAsyncService.class.getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH);

        if (credentialsInputStream == null) {
            throw new FileNotFoundException("File not found: " + CREDENTIALS_FILE_PATH);
        }

        GoogleCredentials credentials = GoogleCredentials.fromStream(credentialsInputStream);

        // Create a storage object using the loaded credentials
        return StorageOptions.newBuilder().setCredentials(credentials).build().getService();
    }

    @Async
    public  void uploadFileToBucket( File uploadFile,Long chunkId) throws GeneralSecurityException, IOException {
        String bucketName = "nimble-sweep-407019-10-45";
        Storage storage = getStorage();
        try {
            File file = uploadFile;
            Blob blob = storage.create(BlobInfo.newBuilder(bucketName, file.getName()).build(), new FileInputStream(file));
            System.out.println("File " + file.getName() + " uploaded to bucket " + bucketName);
            BackupChunk backupChunkUpate = backupChunkRepository.findById(chunkId).get();
            backupChunkUpate.setStatus(true);
            backupChunkRepository.save(backupChunkUpate);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
