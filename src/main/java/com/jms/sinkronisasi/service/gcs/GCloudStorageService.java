package com.jms.sinkronisasi.service.gcs;


import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Service
public class GCloudStorageService {
    private static final String CREDENTIALS_FILE_PATH = "cred.json";
    private static Storage  getStorage() throws IOException, GeneralSecurityException {
        InputStream credentialsInputStream = GCloudStorageService.class.getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH);

        if (credentialsInputStream == null) {
            throw new FileNotFoundException("File not found: " + CREDENTIALS_FILE_PATH);
        }

        GoogleCredentials credentials = GoogleCredentials.fromStream(credentialsInputStream);

        // Create a storage object using the loaded credentials
        return StorageOptions.newBuilder().setCredentials(credentials).build().getService();
    }


    public static void createBucketGcs() throws GeneralSecurityException, IOException {
        // Initialize Google Cloud Storage
        Storage storage = getStorage();

        // Create a bucket
        String bucketName = "nimble-sweep-407019-10-45";
        Bucket bucket = storage.create(BucketInfo.of(bucketName));
        System.out.println("Bucket created: " + bucket.getName());
    }

    public  void uploadFileToBucket( File uploadFile) throws GeneralSecurityException, IOException {
        String bucketName = "nimble-sweep-407019-test-upload-bro-dan-sis";
        Storage storage = getStorage();
        try {
            File file = uploadFile;
            Blob blob = storage.create(BlobInfo.newBuilder(bucketName, file.getName()).build(), new FileInputStream(file));
            System.out.println("File " + file.getName() + " uploaded to bucket " + bucketName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
