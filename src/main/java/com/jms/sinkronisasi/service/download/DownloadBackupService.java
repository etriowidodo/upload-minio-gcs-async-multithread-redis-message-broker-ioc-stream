package com.jms.sinkronisasi.service.download;

import com.jms.sinkronisasi.local.model.dao.DownloadBackupChunk;
import com.jms.sinkronisasi.local.model.repository.DownloadBackupChunkRepository;
import com.jms.sinkronisasi.service.minio.MinioService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.IntStream;

import static com.jms.sinkronisasi.constant.Constants.BACKUP_PATH_MINIO;
import static com.jms.sinkronisasi.constant.Constants.FILE_NAME;
import static jdk.nashorn.internal.objects.NativeMath.log;

@Service
public class DownloadBackupService {
    @Value("${backup.db.download.directory}")
    String pathDownload;
    @Autowired
    MinioService minioService;
    @Autowired
    DownloadBackupChunkRepository downloadBackupChunkRepository;
    @Value("${backup.db.host}")
    String hostname;
    @Value("${spring.datasource.username}")
    String username;
    @Value("${backup.db.name}")
    String dbName;
    @Value("${backup.db.port}")
    String dbPort;
    @Value("${backup.db.directory}")
    String backupDirectory;
    @Value("${spring.datasource.password}")
    String password;
    @Value("${backup.db.dump.location}")
    String pgdum;
    @Async
    public void downloadBackupMinio(Integer count,String bucketName,String path) throws IOException {
        String fullDownloadDirectory = pathDownload+"/"+bucketName;
        File directoryChunk = new File(fullDownloadDirectory);
        if (!directoryChunk.exists()) {
            boolean created = directoryChunk.mkdirs();
            if (created) {
                System.out.println("Backup directory created successfully.");
            }
        }
        downloadBackupChunkRepository.deleteAll();
        IntStream.range(0, count).parallel().forEach(index -> {
            DownloadBackupChunk downloadBackupChunk = new DownloadBackupChunk();
            downloadBackupChunk.setBucketName(bucketName);
            downloadBackupChunk.setPart(index);
            downloadBackupChunkRepository.save(downloadBackupChunk);
        });


        downloadBackupChunkRepository.findByStatusOrderByPartAsc(false)
                .parallelStream()
                .forEach(chunk -> {
                    DownloadBackupChunk downloadBackupChunk = downloadBackupChunkRepository.findById(chunk.getId()).get();
                    String part = String.valueOf(downloadBackupChunk.getPart());
                    var download  = minioService.downloadBackupFile(
                            downloadBackupChunk.getBucketName()
                            ,path
                            ,part);
                    if(download){
                        String fullFileName = FILE_NAME + part;
                        downloadBackupChunk.setStatus(true);
                        downloadBackupChunk.setFullFilename(fullFileName);
                        downloadBackupChunk.setPathUpload(fullDownloadDirectory);
                        downloadBackupChunk.setFileName(BACKUP_PATH_MINIO);
                        downloadBackupChunkRepository.save(downloadBackupChunk);
                    }

                });
        mergeFile(fullDownloadDirectory);
        RestoreDb(fullDownloadDirectory,bucketName);
    }

    public String mergeFile(String fullDownloadDirectory) {
        String fileName = BACKUP_PATH_MINIO;
        Path assembledFilePath = Paths.get(fullDownloadDirectory, fileName);

        try (FileOutputStream fos = new FileOutputStream(assembledFilePath.toFile())) {
            for (int i = 0; ; i++) {
                File chunkFile = new File(fullDownloadDirectory, fileName + "_part_" + i);
                if (!chunkFile.exists()) {
                    break;
                }
                byte[] chunkData = Files.readAllBytes(chunkFile.toPath());
                fos.write(chunkData);
                chunkFile.delete(); // Delete the chunk after assembling
            }
            System.out.println("File assembled successfully");
            return "File assembled successfully";
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Failed to assemble file");
            return "Failed to assemble file";
        }
    }

    public void RestoreDb(String fullDownloadDirectory,String bucketName) throws IOException {
        String fileName = BACKUP_PATH_MINIO;
        String pathRestore = fullDownloadDirectory+"/"+fileName;
        Runtime r = Runtime.getRuntime();
        Process p;
        ProcessBuilder pb;
        InputStreamReader reader;
        BufferedReader buf_reader;
        String line;
        String dbase = dbName+"_"+bucketName.replace(".", "_");;


        r = Runtime.getRuntime();
        pb = new ProcessBuilder(pgdum + "psql.exe","-h", hostname, "-p", dbPort, "-U", username, "-c"," DROP database IF EXISTS "+dbase);
        pb.environment().put("PGPASSWORD", password);
        pb.redirectErrorStream(true);
        p = pb.start();
        try {
            InputStream is = p.getInputStream();
            OutputStream out = p.getOutputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String ll;
            while ((ll = br.readLine()) != null) {
                        System.out.println(ll);

            }
        } catch (IOException e) {
            log("ERROR " + e.getMessage(), e);
        }

        r = Runtime.getRuntime();
        pb = new ProcessBuilder(pgdum + "psql.exe","-h", hostname, "-p", dbPort, "-U", username, "-c"," Create database "+dbase);
        pb.environment().put("PGPASSWORD", password);
        pb.redirectErrorStream(true);
        p = pb.start();
        try {
            InputStream is = p.getInputStream();
            OutputStream out = p.getOutputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String ll;
            while ((ll = br.readLine()) != null) {
                System.out.println(ll);

            }
        } catch (IOException e) {
            log("ERROR " + e.getMessage(), e);
        }

        r = Runtime.getRuntime();
        pb = new ProcessBuilder(pgdum + "pg_restore.exe","-h", hostname, "-p", dbPort, "-U", username, "-d",dbase,"-v",pathRestore);
        pb.environment().put("PGPASSWORD", password);
        pb.redirectErrorStream(true);
        p = pb.start();
        try {
            InputStream is = p.getInputStream();
            OutputStream out = p.getOutputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String ll;
            while ((ll = br.readLine()) != null) {
                        System.out.println(ll);

            }
        } catch (IOException e) {
            log("ERROR " + e.getMessage(), e);
        }

    }

}
