package com.jms.sinkronisasi.service.minio;

import com.jms.sinkronisasi.local.model.dao.BackupChunk;
import com.jms.sinkronisasi.local.model.repository.BackupChunkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPOutputStream;

@Service
public class FileService {
    private static final int CHUNK_SIZE = 4194304;
    @Autowired
    MinioService minioService;
    @Autowired
    BackupChunkRepository backupChunkRepository;
    public void chunkFile(String filePath,String fileName,String pathDirChunk,String bucketName){
//        try {
//            compressFile("E:\\backup\\backup.sql","E:\\backup\\10.45\\");
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        File fileToSend = new File(filePath);
        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(fileToSend))) {
            long contentLength = fileToSend.length();
            int totalChunks = (int) Math.ceil((double) contentLength / CHUNK_SIZE);

            byte[] buffer = new byte[CHUNK_SIZE];
            int bytesRead;
            int chunkIndex = 0;
            backupChunkRepository.deleteAll();
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byte[] chunkData = new byte[bytesRead];
                System.arraycopy(buffer, 0, chunkData, 0, bytesRead);

                int finalChunkIndex = chunkIndex; // to use inside lambda expression
                CompletableFuture.runAsync(() -> createCunkfile(fileName,pathDirChunk, finalChunkIndex,chunkData,bucketName), executorService);

                chunkIndex++;
            }

            System.out.println("File sent asynchronously!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown(); // shutdown executor service when done
        }
        //uploadChunkMinioService.uploadChunkToMinio();
    }

    public String createCunkfile(String fileName,String pathDirChunk,int chunkIndex, byte[] chunkData,String bucketName) {

        try {
            String fullFileName = fileName + "_part_" + chunkIndex;
            File chunkFile = new File(pathDirChunk,fullFileName );

            try (FileOutputStream fos = new FileOutputStream(chunkFile)) {
                fos.write(chunkData);
            }
                BackupChunk backupChunk = new BackupChunk();
                    backupChunk.setBucketName(bucketName);
                    backupChunk.setFileName(fileName);
                    backupChunk.setPart(chunkIndex);
                    backupChunk.setPathUpload(pathDirChunk);
                    backupChunk.setFullFilename(fullFileName);
                    backupChunk.setStatus(false);
                backupChunkRepository.save(backupChunk);
            return "Chunk " + chunkIndex + " received successfully";
        } catch (IOException e) {
            e.printStackTrace();
            return "Failed to receive chunk " + chunkIndex;
        }

    }


    public double checkProgressBackup() {
        long totalCount = backupChunkRepository.getTotalCount();
        long trueCount = backupChunkRepository.getTrueCount();

        if (totalCount == 0) {
            return 0.0; // To avoid division by zero error
        }

        return ((double) trueCount / totalCount) * 100;
    }

    public static void compressFile(String sourceFile, String targetFile) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(sourceFile);
        FileOutputStream fileOutputStream = new FileOutputStream(targetFile);
        GZIPOutputStream gzipOutputStream = new GZIPOutputStream(fileOutputStream);

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = fileInputStream.read(buffer)) > 0) {
            gzipOutputStream.write(buffer, 0, bytesRead);
        }

        fileInputStream.close();
        gzipOutputStream.finish();
        gzipOutputStream.close();
    }


}
