package com.jms.sinkronisasi.service.minio;

import com.jms.sinkronisasi.local.model.dao.BackupChunk;
import com.jms.sinkronisasi.local.model.repository.BackupChunkRepository;
//import com.jms.sinkronisasi.service.gcs.GCloudStorageService;
import com.jms.sinkronisasi.service.gcs.GCloudStorageUploadFileAsyncService;
//import com.jms.sinkronisasi.service.gdrive.GdriveService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

@Service
public class UploadChunkGdriveService {
    @Autowired
    BackupChunkRepository backupChunkRepository;
//    @Autowired
//    GdriveService gdriveService;

    @Autowired
    GCloudStorageUploadFileAsyncService gCloudStorageUploadFileAsyncService;

    @Async
    public void uploadChunkToGdrive(){
        backupChunkRepository.findByStatusOrderByPartAsc(false)
                .parallelStream() // Execute in parallel
                .forEach(chunk -> {
                    String pathUploadFile = chunk.getPathUpload() + File.separator + chunk.getFullFilename();
                    File uploadFIle = new File(pathUploadFile);
                    try {
                        gCloudStorageUploadFileAsyncService.uploadFileToBucket(uploadFIle, chunk.getId());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    } catch (GeneralSecurityException e) {
                        throw new RuntimeException(e);
                    }
                });
        System.out.println("Selesai Di Upload Semua nya dah");
    }


}
