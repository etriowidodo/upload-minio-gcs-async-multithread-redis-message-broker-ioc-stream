package com.jms.sinkronisasi.service.minio;


import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static com.jms.sinkronisasi.constant.Constants.FILE_NAME;

@Service
public class MinioService {
    private final MinioClient minioClient;


    @Value("${backup.db.download.directory}")
    String pathDownload;
    public MinioService(@Value("${minio.endpoint}") String minioEndpoint,
                        @Value("${minio.accessKey}") String accessKey,
                        @Value("${minio.secretKey}") String secretKey) throws InvalidKeyException, NoSuchAlgorithmException {
        // Initialize Minio client
        this.minioClient = MinioClient.builder()
                .endpoint(minioEndpoint) // Replace with your MinIO endpoint
                .credentials(accessKey, secretKey) // Replace with your MinIO credentials
                .build();
    }
    public void createBucket(String bucketName) throws Exception {
        try {
            // List existing buckets
            List<Bucket> buckets = minioClient.listBuckets();
            boolean bucketExists = buckets.stream().anyMatch(bucket -> bucket.name().equals(bucketName));
            if (!bucketExists) {
                // Create the bucket
                minioClient.makeBucket(MakeBucketArgs.builder()
                        .bucket(bucketName)
                        .build());
            }
        } catch (MinioException e) {
            // Handle Minio-specific exceptions
            throw new Exception("Error occurred while creating bucket: " + e.getMessage());
        } catch (Exception e) {
            // Handle other exceptions
            throw new Exception("Error occurred while creating bucket: " + e.getMessage());
        }
    }


    public void uploadFile(String bucketName, String fileName, MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket(bucketName)
                            .object(fileName)
                            .stream(inputStream, inputStream.available(), -1)
                            .contentType(file.getContentType())
                            .build()
            );
        } catch (Exception e) {
            // Handle exceptions
            e.printStackTrace();
        }
    }

    public boolean uploadFileBoolean(String bucketName,File fileRecive) {
        try {

            File files = fileRecive;

            // Upload each file to MinIO
            if (files != null) {
                    minioClient.uploadObject(
                            UploadObjectArgs.builder()
                                    .bucket(bucketName) // Replace with your bucket name
                                    .object("/backup_database_cms/"+files.getName()) // Use file name as object name
                                    .filename(files.getAbsolutePath())
                                    .build()
                    );
                    System.out.println("File " + files.getName() + " uploaded successfully.");
            }
            return true; // Successfully uploaded
        } catch (ErrorResponseException e) {
            // Handle ErrorResponseException
            e.printStackTrace();
        } catch (InvalidResponseException e) {
            // Handle InvalidResponseException
            e.printStackTrace();
        } catch (MinioException | IOException e) {
            // Handle MinioException or IOException
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        }
        return false; // Upload failed
    }
    public List<String> listFiles(String bucketName, String path) throws MinioException {
        try {
            List<String> fileNames = new ArrayList<>();
            Iterable<Result<Item>> results = minioClient.listObjects(
                    ListObjectsArgs.builder()
                            .bucket(bucketName)
                            .prefix(path) // Path within the bucket
                            .recursive(true)
                            .build()
            );

            for (Result<Item> result : results) {
                Item item = result.get();
                fileNames.add(item.objectName());
            }

            return fileNames;
        } catch (Exception e) {
            // Handle exceptions
            e.printStackTrace();
            throw new MinioException("Error listing files: " + e.getMessage());
        }
    }

    public void downloadFile(String bucketName, String objectName, String destinationPath) throws MinioException {
        try {
            InputStream inputStream = getObject(bucketName,objectName);
            FileOutputStream fileOutputStream = new FileOutputStream(destinationPath);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = inputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, length);
            }

            fileOutputStream.close();
            inputStream.close();
        } catch (Exception e) {
            // Handle exceptions
            e.printStackTrace();
            throw new MinioException("Error downloading file: " + e.getMessage());
        }
    }

    public InputStream getObject(String bucketName, String objectName) throws MinioException {
        try {
            // Fetch the object as an InputStream
            return minioClient.getObject(
                    GetObjectArgs.builder()
                            .bucket(bucketName)
                            .object(objectName)
                            .build()
            );
        } catch (Exception e) {
            // Handle exceptions
            throw new MinioException("Error downloading file: " + e.getMessage());
        }
    }

    public int countFilesInPath(String bucketName, String path) {
        try {

            Iterable<Result<Item>> results = minioClient.listObjects(
                    ListObjectsArgs.builder()
                            .bucket(bucketName)
                            .prefix(path)
                            .recursive(true) // Set to true to include objects in subfolders
                            .build()
            );

            int fileCount = 0;

            // Iterate through the objects and count the files
            for (Result<Item> result : results) {
                Item item = result.get();
                if (!item.isDir()) {
                    fileCount++;
                }
            }

            return fileCount;
        } catch (Exception e) {
            e.printStackTrace();
            // Handle exceptions
            return -1; // Return -1 to indicate an error
        }
    }

    public boolean downloadBackupFile(String bucketName, String path, String part) {
        String objectName = path + "/" + FILE_NAME + part;
        String fullDownloadDirectory = pathDownload+"/"+bucketName;
        String fileName = FILE_NAME + part;
        Path downloadPath = Paths.get(fullDownloadDirectory, fileName);
        GetObjectArgs getObjectArgs = GetObjectArgs.builder()
                .bucket(bucketName)
                .object(objectName)
                .build();
        try (InputStream stream = minioClient.getObject(getObjectArgs);
             FileOutputStream fileOutputStream = new FileOutputStream(downloadPath.toFile())) {

            byte[] buffer = new byte[1024];
            int length;
            while ((length = stream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, length);
            }
            System.out.println("File downloaded successfully to: " + downloadPath);
            return true; // Return true when download succeeds

        } catch (IOException | MinioException | InvalidKeyException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false; // Return false if an exception occurs
        }
    }




}
