package com.jms.sinkronisasi.service.minio;

import com.jms.sinkronisasi.local.model.dao.BackupChunk;
import com.jms.sinkronisasi.local.model.repository.BackupChunkRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class UploadChunkMinioService {
    @Autowired
    BackupChunkRepository backupChunkRepository;
    @Autowired
    MinioService minioService;

    @Async
    public void uploadChunkToMinio(){
        backupChunkRepository.findByStatusOrderByPartAsc(false)
                .parallelStream() // Execute in parallel
                .forEach(chunk -> {

                    String pathUploadFile = chunk.getPathUpload() + File.separator + chunk.getFullFilename();
                    File uploadFIle = new File(pathUploadFile);
                    var uploadSuccess = minioService.uploadFileBoolean(
                            chunk.getBucketName(),
                            uploadFIle
                    );
                    if(uploadSuccess){
                        BackupChunk backupChunkUpate = backupChunkRepository.findById(chunk.getId()).get();
                        backupChunkUpate.setStatus(true);
                        backupChunkRepository.save(backupChunkUpate);
                        //System.out.println(chunk.getId()+"->Succsess");
                    }else{
                        System.out.println(chunk.getId()+"->gagal");
                    }
                });
    }


}
